import { jwtDecode } from 'jwt-decode'
import axios from 'axios'

export const getUserInfo = (token: any) => {
  const config = {
    headers: {
      "Content-type": "application/json",
      "Authorization" : "Bearer " + token
    },
  };
  const decodedToken = jwtDecode(token)
  const userId = decodedToken.sub
  return axios.get("http://localhost:8080/users/"+userId, config)
}

export const updateUserInfo = async (token: any, username: any, email: any) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      params: {
        email: email,
        username: username
      }
    };

    const response = await axios.put('http://localhost:8080/users/edit', null, config);
    return response.data
  } catch (error: any) {
    console.error('Error updating UserInfo:', error);
    const errorMessage = error.response ? error.response.data : 'An error occurred';
    throw new Error(errorMessage); // Propagate the error to the caller    }
  }
}
export const putPasswordInfo = async (token: any, newPassword: any) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      params: {
        password: newPassword,
      }
    };
    return await axios.put('http://localhost:8080/users/editPassword', null, config);

  } catch (error) {
    console.error('Error creating trivio:', error);
    throw error; // Propagate the error to the caller
  }
};

export const getUserInfo1 = async (token: any) => {
  try {
    const config = {
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer " + token
      },
    };
    const decodedToken = jwtDecode(token)
    const response = await axios.get("http://localhost:8080/users", config)
    return response.data
  } catch (error: any) {
    console.error('Error updating UserInfo:', error);
    const errorMessage = error.response ? error.response.data : 'An error occurred';
    throw new Error(errorMessage); // Propagate the error to the caller    }
  }
}

export const createUser = async (username: string, password: string, email: string) => {
  try {
    const data= {
      username: username,
      password: password,
      email: email
    }

    return await axios.post('http://localhost:8080/signup', data);
  } catch (e) {
    return "Failed to create user in backend"
  }
}

