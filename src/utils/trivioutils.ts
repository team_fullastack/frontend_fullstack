import axios from 'axios'
export const getTrivioById =  (token: any, id: any) => {
  const config = {
    headers: {
      "Content-type": "application/json",
      "Authorization": "Bearer " + token
    },
  };
  console.log(token)
  console.log(id)
  try {
    return axios.get(`http://localhost:8080/trivios/${id}`, config)
  } catch (error) {
    console.error('Error fetching quiz data:', error);
  }
}
export const getTriviosByUser = (token: any, page: number, size: number, category: any, difficulty: any, tags:any) => {
  try {
    const config: any = {
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer " + token
      },
      params: {
        'page': page,
        'size': size
      }
    };

    // Conditionally add category, difficulty, and tags to the params object
    if (category !== null) {
      config.params['category'] = category;
    }
    if (difficulty !== null) {
      config.params['difficulty'] = difficulty;
    }

    if(tags !== null){
      const nonEmptyTags = tags.filter(tag => tag.tag.trim() !== "");

      //Check if there is any valid tags at all
      if(nonEmptyTags.length !== 0) {
        const tagString = nonEmptyTags.map(tag => tag.tag).join(',');
        config.params['tagString'] = tagString;
      }
    }

    console.log(config);
    return axios.get(`http://localhost:8080/trivios/user`, config)
  } catch (error) {
    console.log(error)
  }
}
export const postNewTrivio = async (token: any, trivioData: any) => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    }; await axios.post('http://localhost:8080/trivios/create', trivioData, config);

  } catch (error) {
    console.error('Error creating trivio:', error);
    throw error;
  }
};

export const editTrivio = async(token:any, trivioData:any, trivioId:any)=>{
  try{
    const config = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    };
    const response = await axios.put(`http://localhost:8080/trivios/edit/${trivioId}`, trivioData, config);
    return response.data

  }catch (error: any){
    console.error('Error updating UserInfo:', error);
    const errorMessage = error.response ? error.response.data : 'An error occurred';
    throw new Error(errorMessage); // Propagate the error to the caller    }
  }
}

export const deleteTrivio = async (token: string, id) => {
  try {
    const config = {
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer " + token
      },
    };
    const response = await axios.delete(`http://localhost:8080/trivios/delete/${id}`,config);
    return response.data
  } catch (err: any) {
    console.log(err)
    const errorMessage = err.response ? err.response.data : 'An error occurred';
    throw new Error(errorMessage); // Propagate the error to the caller    }
  }
}

export const getSharedTriviosByUser = (token: any, page: number, size: number, category: any, difficulty: any, tags:any) => {
  try {
    const config: any = {
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer " + token
      },
      params: {
        'page': page,
        'size': size
      }
    };

    // Conditionally add category, difficulty, and tags to the params object
    if (category !== null) {
      config.params['category'] = category;
    }
    if (difficulty !== null) {
      config.params['difficulty'] = difficulty;
    }

    if(tags !== null){
      const nonEmptyTags = tags.filter(tag => tag.tag.trim() !== "");

      //Check if there is any valid tags at all
      if(nonEmptyTags.length !== 0) {
        const tagString = nonEmptyTags.map(tag => tag.tag).join(',');
        config.params['tagString'] = tagString;
      }
    }

    console.log(config);
    return axios.get(`http://localhost:8080/trivios/shared`, config)
  } catch (error) {
    console.log(error)
  }
}

export const removeEditor = async(token:any, trivioId:any, username:any)=>{
  try{
    const config = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      params:{
        'username':username,
        'trivioId': trivioId
      }
    };
    await axios.put(`http://localhost:8080/trivios/remove-user`, null ,config);
    console.log("editor removed")

  }catch (error){
    console.error('Error editing trivio:', error);
    throw error;
  }
}

export const addEditor = async(token:any, trivioId:any, username:any)=>{
  try{
    const config = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      params:{
        'username':username,
        'trivioId': trivioId
      }
    };
    const response = await axios.put(`http://localhost:8080/trivios/add-user`, null ,config);
    return response.data
  }catch (error: any){
    console.error('Error updating UserInfo:', error);
    const errorMessage = error.response ? error.response.data : 'An error occurred';
    throw new Error(errorMessage); // Propagate the error to the caller    }
  }
}

export const getTriviosByOtherUser = (token: any, page: number, size: number, category: any, difficulty: any, tags:any) => {
  try {
    const config: any = {
      headers: {
        "Content-type": "application/json",
        "Authorization": "Bearer " + token
      },
      params: {
        'page': page,
        'size': size
      }
    };

    // Conditionally add category, difficulty, and tags to the params object
    if (category !== null) {
      config.params['category'] = category;
    }
    if (difficulty !== null) {
      config.params['difficulty'] = difficulty;
    }

    if(tags !== null){
      const nonEmptyTags = tags.filter(tag => tag.tag.trim() !== "");

      //Check if there is any valid tags at all
      if(nonEmptyTags.length !== 0) {
        const tagString = nonEmptyTags.map(tag => tag.tag).join(',');
        config.params['tagString'] = tagString;
      }
    }

    console.log(config);
    return axios.get(`http://localhost:8080/trivios/discover`, config)
  } catch (error) {
    console.log(error)
  }
}

