import axios from 'axios'

export const uploadPicture = async (file: File, token: String) => {
  try {
    const formData = new FormData();
    console.log(file)
    formData.append('file', file);
    const response = await axios.post('http://localhost:8080/upload', formData, {
      headers: {
        'Content-Type': file.type === 'image/jpeg' ? 'image/jpeg' : 'image/png',
        'Authorization': `Bearer ${token}`
      }
    });
    return response.data; // Assuming the response contains the URL of the uploaded picture
  } catch (error) {
    throw new Error('Failed to upload picture');
  }
};

export const getPicture = async (imageName: String, token: String) => {
  try {
    // Fetch the image from the backend using the /images/{imageName} endpoint
    const response = await axios.get(`http://localhost:8080/${imageName}`, {
      responseType: 'blob', // Set the response type to blob to handle binary data
      headers: {
        'Authorization': `Bearer ${token}`
      }
    });

    // Create a blob URL for the image
    return URL.createObjectURL(response.data)
    // Now you can use this imageUrl to display the image in your frontend

  } catch (error) {
    console.error('Error fetching image:', error);
    // Handle the error, display a message to the user, etc.
  }
};