import axios from 'axios'

export const postMessage = async (token: any, form: any) => {
  try{
    const config = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    };
    console.log(form)
    return await axios.post('http://localhost:8080/messages',form, config)
  } catch (error){
    console.error('Error sending message:', error)
  }
}
