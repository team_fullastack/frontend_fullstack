import axios from 'axios'
import { jwtDecode } from 'jwt-decode'

export const getTrivioResultTitles =  (token: any) => {
  const config = {
    headers: {
      "Content-type": "application/json",
      "Authorization": "Bearer " + token
    },
  };
  try {
    return axios.get(`http://localhost:8080/results/trivioTitles`, config)
  } catch (error) {
    console.error('Error fetching quiz data:', error);
  }
}

export const getHistoryByUser = (token: any, title: String, page:number, size:number ) => {
  const config = {
    headers: {
      "Content-type": "application/json",
      "Authorization" : "Bearer " + token
    },
    params: {
      'page': page,
      'size': size
    }
  };
  if(title != null){
    config.params['title'] = title;
  }

  console.log(config)

  const decodedToken = jwtDecode(token)
  const userId = decodedToken.sub
  return axios.get("http://localhost:8080/results?userId="+userId, config)
}
