  import axios from "axios";
import { defineStore } from "pinia";
import {getJWTToken} from "@/utils/tokenutils";
import { jwtDecode } from "jwt-decode";
  import router from "@/router";
  import {useRouter} from "vue-router";
  import {markRaw} from "vue";

export const useTokenStore = defineStore( {
    id: 'token',
    state: () => ({
        jwtToken: "",
        loggedInUser: "",
        password: "",
        tokenExpirationTimer: null as number | null,
        router: markRaw(router), // Add Vue Router instance to the store
    }),

    persist: {
        storage: sessionStorage, // note that data in sessionStorage is cleared when the page session ends
    },

    actions: {
        async getTokenAndSaveInStore(username: string, password: string) {
            try{
                console.log(username)
                let response = await getJWTToken(username, password);
                let data = response.data
                this.jwtToken = ''
                console.log(response.data)
                const decoded = jwtDecode(response.data)
                if(data != null && data !== '' && data !== undefined && decoded !== undefined){
                    const currentTime = Math.floor(Date.now() / 1000);
                    const timeToExpiration = decoded.exp - (currentTime+5)
                    if(timeToExpiration>0){
                        this.jwtToken=data
                        this.loggedInUser=username
                        this.password=password
                        this.refreshTokenExpirationTimer(timeToExpiration);
                    }
                }


            } catch (err){
                console.log(err)
            }
        },
        async refreshToken(username: string, password:string) {
            try {
                // Make a request to the backend API to refresh the token
                const response = await getJWTToken(username, password)
                this.jwtToken = response.data
                // Refresh the expiration timer with the new token's expiration time
                const decoded = jwtDecode(this.jwtToken);
                const currentTime = Math.floor(Date.now() / 1000);
                const timeToExpiration = decoded.exp - (currentTime+5);
                this.refreshTokenExpirationTimer(timeToExpiration);
            } catch (err) {
                console.log(err);
            }
        },

        refreshTokenExpirationTimer(timeToExpiration: number) {
            // Clear existing timer if it exists
            if (this.tokenExpirationTimer !== null) {
                clearTimeout(this.tokenExpirationTimer);
            }
            // Start new timer to alert user when token is about to expire
            this.tokenExpirationTimer = setTimeout(async () => {
                if (confirm('Token is about to expire. Do you want to refresh?')) {
                    console.log(this.tokenExpirationTimer)
                    await this.refreshToken(this.loggedInUser, this.password);
                    // window.location.reload();
                } else {
                    await router.push("/")
                    console.log('Token expiration timer stopped.');
                }     // Call token refresh method
            }, timeToExpiration * 1000); // Convert seconds to milliseconds
        },

    },
    getters: {
        isTokenValid(): boolean {
            if (!this.jwtToken) return false; // Token doesn't exist
            const decoded: { exp?: number } | null = jwtDecode(this.jwtToken);
            if (!decoded || !decoded.exp) return false; // Token is invalid or expired
            const currentTime = Math.floor(Date.now() / 1000);
            return decoded.exp > currentTime; // Return true if token is valid
        }
    },
});
