import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '@/views/frontpage/LoginView.vue'
import FrontPageView from '@/views/FrontPageView.vue'
import SignUpView from '@/views/frontpage/SignUpView.vue'
import ProfileView from '@/views/mainpage/ProfileView.vue'
import DiscoveryView from '@/views/mainpage/DiscoveryView.vue'
import MyTriviosView from '@/views/mainpage/MyTriviosView.vue'
import ContactView from '@/views/mainpage/ContactView.vue'
import HistoryView from '@/views/mainpage/HistoryView.vue'
import CreateTrivio from '@/views/mainpage/CreateTrivio.vue'
import MainPageView from '@/views/MainPageView.vue'
import StartView from '@/views/mainpage/StartView.vue'
import PlayView from '@/views/mainpage/PlayView.vue'
import EditTrivioView from '@/views/mainpage/EditTrivioView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'mainpage',
      component: FrontPageView,
      children: [
        {
          path: '',
          component: LoginView
        },
        {
          path: 'createaccount',
          component: SignUpView
        },
        {
          path: 'login',
          component: LoginView
        }
      ]
    },
    {
      path: '/homepage',
      name: 'homepage',
      component: MainPageView,
      children: [
        {
          path: 'profile',
          component: ProfileView
        },
        {
          path: 'create-trivio',
          component: CreateTrivio
        },
        {
          path: 'discovery',
          component: DiscoveryView
        },
        {
          path: 'trivios',
          component: MyTriviosView
        },
        {
          path: 'contact',
          component: ContactView
        },
        {
          path: 'history',
          component: HistoryView
        },
        {
          name: 'start',
          path: 'start/:id',
          component: StartView
        },
        {
          path: 'start/:id/play',
          name: 'play',
          component: PlayView
        },
        {
          path: 'start/:id/edit',
          name: 'edit',
          component: EditTrivioView
        }
      ]
    }
  ]
})

export default router
