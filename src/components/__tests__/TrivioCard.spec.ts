import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils';
import TrivioCard from '@/components/TrivioCard.vue';

describe('TrivioCard', () => {
  it('renders props correctly', () => {
    // Define props
    const props = {
      title: 'Sample Quiz',
      image: '/path/to/image.jpg',
      category: 'Sample Category',
      numberOfQuestion: 10,
      username: 'sampleUser',
      difficulty: 'Easy',
      media: '/path/to/media.jpg',
    };

    // Mount the component with props
    const wrapper = mount(TrivioCard, {
      props,
    });

    // Assert that props are rendered correctly
    expect(wrapper.find('.title').text()).toBe(props.title);
    expect(wrapper.find('.category').text()).toContain(props.category);
    expect(wrapper.find('.question').text()).toContain(`${props.numberOfQuestion} Questions`);
    expect(wrapper.find('.user').text()).toBe(props.username);
    expect(wrapper.find('.difficulty').text()).toBe(props.difficulty);
    expect(wrapper.find('img').attributes('src')).toBe(props.media);
  });

  it('renders default image if image prop is not provided', () => {
    // Mount the component without image prop
    const wrapper = mount(TrivioCard, {
      props: {
        title: 'Sample Quiz',
        category: 'Sample Category',
        numberOfQuestion: 10,
        username: 'sampleUser',
        difficulty: 'Easy',
      },
    });

    // Assert that default image is rendered
    expect(wrapper.find('img').attributes('src')).toBe('/src/assets/trivio.svg');
  });
});
