import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils';
import HistoryBox from '@/components/HistoryBox.vue';  

describe('HistoryBox', () => {
  it('renders with correct props', () => {
    // Mock props
    const props = {
      title: 'Sample Quiz',
      image: '/sample-image.png',
      numberOfQuestion: 10,
      username: 'John Doe',
      difficulty: 'Easy',
      score: 8,
      date: '2024-04-10',
    };

    // Mount the component with mock props
    const wrapper = mount(HistoryBox, {
      props,
    });

    // Assert that the rendered component contains the correct information
    expect(wrapper.find('.quiz-title').text()).toBe(props.title);
    expect(wrapper.find('.difficulty').text()).toContain(`Difficulty: ${props.difficulty}`);
    expect(wrapper.find('.username').text()).toBe(props.username);
    expect(wrapper.find('.score-text').text()).toContain(`${props.score}/${props.numberOfQuestion}`);
    expect(wrapper.find('.date').text()).toBe(props.date);
    
    // Assert image source
    const imageSrc = wrapper.find('.quiz-box img').attributes('src');
    if (props.image) {
      expect(imageSrc).toBe(props.image);
    } else {
      expect(imageSrc).toBe('/src/assets/trivio.svg');
    }
  });
});
