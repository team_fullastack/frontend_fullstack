import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils';
import UserSchema from '@/components/UserSchema.vue';

describe('UserSchema Component', () => {
  it('renders correctly with default props', async () => {
    // Mount the component
    const wrapper = mount(UserSchema);

    // Assert that the component renders correctly
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('h1').text()).toBe('Login');
    expect(wrapper.find('#username').exists()).toBe(true);
    expect(wrapper.find('#password').exists()).toBe(true);
    expect(wrapper.find('#email').exists()).toBe(false); // Email input should not be rendered by default
    expect(wrapper.find('button').text()).toBe('Login');
    expect(wrapper.find('#loginStatus').text()).toBe('');
  });

  it('renders correctly with custom props', async () => {
    // Mount the component with custom props
    const wrapper = mount(UserSchema, {
      props: {
        buttonText: 'Sign Up',
        headerText: 'Create Account',
        status: 'Invalid username or password',
      },
    });

    // Assert that the component renders correctly with custom props
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.find('h1').text()).toBe('Create Account');
    expect(wrapper.find('#username').exists()).toBe(true);
    expect(wrapper.find('#password').exists()).toBe(true);
    expect(wrapper.find('#email').exists()).toBe(true); // Email input should be rendered with custom props
    expect(wrapper.find('button').text()).toBe('Sign Up');
    expect(wrapper.find('#loginStatus').text()).toBe('Invalid username or password');
  });
});
