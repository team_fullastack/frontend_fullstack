import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils';
import QuestionBox from '@/components/QuestionBox.vue';

describe('QuestionBox', () => {
  it('renders props correctly', () => {
    // Define props
    const props = {
      image: '/path/to/image.jpg',
      questionNumber: 1,
      tags: ['tag1', 'tag2'],
      question: ['Sample question line 1', 'Sample question line 2'],
    };

    // Mount the component with props
    const wrapper = mount(QuestionBox, {
      props,
    });

    // Assert that props are rendered correctly
    expect(wrapper.find('img').attributes('src')).toBe(props.image);
    expect(wrapper.find('label[for="number"]').text()).toBe(props.questionNumber.toString());
    
    // Construct the expected question string
    const expectedQuestion = JSON.stringify(props.question, null, 2); // Use JSON.stringify
    const receivedQuestion = wrapper.find('label[for="question"]').text();
    expect(receivedQuestion).toBe(expectedQuestion);
  });

  it('renders default image if image prop is not provided', () => {
    const wrapper = mount(QuestionBox, {
      props: {
        questionNumber: 1,
        tags: ['tag1', 'tag2'],
        question: ['Sample question line 1', 'Sample question line 2'],
      },
    });

    expect(wrapper.find('img').attributes('src')).toBe('/src/assets/trivio.svg');
  });
});
