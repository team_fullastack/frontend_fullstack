# Frontend Trivio

The frontend component of the Trivio quiz application is built using Vue.js and Vite. It provides a user interface for interacting with the backend APIs. Unit testing is performed using vitest, while integration testing is conducted using Cypress (today most of these tests are failing due to last minute updates in feedback from backend).

## Content
- [Functionality](#functionality)
- [Security]("#security")
- [Installation](#installation)
- [Usage](#usage)
- [Tests](#tests)
- [Dependencies](#dependencies)
- [Future work](#future-work)
- [Authors](#authors)

## Functionality
User
- Signup and login
- Discover quizzes made by other users
- Take quizzes and score imidiate scores. 
- Watch previous quiz scores
- Create quizzes.
- Collaborate on quizzes
- Contact admin 
- Change username and password

Admin 
- Access to all users
- See all trivios 

## Security
Trivio uses JWT-Tokens for user authentication during API calls. Upon user login, the backend validates the provided email and encoded password comparing them with existing user records. A JWT token is generated using JWTSBuilder, incorporating the user's id and expiration time. This token serves as the user's authentication credential for making API calls. 

## Installation
1. Make sure you have Node.js and npm installed

2. Clone the repository 
```bash 
git clone https://gitlab.stud.idi.ntnu.no/team_fullastack/frontend_fullstack.git
```

3. Navigate to frontend_fullstack:
```bash
cd frontend_fullstack
```

4. Install dependencies:
```bash
npm install
```

## Usage 
1. Run the frontend application.
```bash
npm run dev
```
Now the application will be accessible from http://localhost:5173

2. Navigate to this URL in your web browser to view the application

## Tests

### Cypress
1. Ensure that both the backend and frontend applications are running.

2. Run Cypress interaction tests:
```bash
npm run test:e2e
```

The tests are sometimes unstable, you might have to run the command several times for them to work

### Vitest 
1. Run Vitest unit tests:
```bash
npm run test:unit
```

## Dependencies
- Vue.js
- Vite
- Axios
- Vue Router
- Pinia
- Cypress
- Vitest

## Future work

### Mobile Responsive UI
Implement a mobile-friendly user interface to ensure optimal user experience across various devices and screen sizes. This involves optimizing layouts, font sizes, and interactive elements for smaller screens.

### Additional Admin Functionalities
Expand the capabilities of the admin dashboard to provide more control and management options.

### Performance 
As the project developed, we adjusted our functionalities to meet requirements and time constraints. Consequently, some aspects of our database queries and server-side logic may not be optimized for performance. To enhance the efficiency of our application, future work will prioritize optimizing these areas, resulting in improved server-side performance.

### Feedback
The application should provide better and more feedback to the user. Todays version notify the user by using alerts. In future work this should be more costumized to the application. The application should also find a better solution to when a toke expires. In todays application the user is notifyed by an alert set by a timer in the tokenstore. This method do have some very critical weaknesses. Some are today solved by refreshing the page. 

## Authors
Gia Hy Nguyen  
Tini Tran  
Vilde Min Vikan  
