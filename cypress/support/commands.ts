/// <reference types="cypress" />

declare global {
    namespace Cypress {
        interface Chainable {
            /**
             * Custom command to log in.
             * @example cy.login('username', 'password')
             */
            login(username: string, password: string): Chainable<void>;
        }
    }
}

Cypress.Commands.add('login', (username, password) => {
    cy.visit('/login') 
    cy.get('#username').type(username)
    cy.get('#password').type(password)
    cy.get('#signinButton').click()
    cy.url().should('include', '/homepage')
})

export { }
