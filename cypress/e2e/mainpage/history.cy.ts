describe('History Page', () => {
    beforeEach(() => {
      cy.login('test', 'password')
      cy.visit('/homepage/history') 
    })
  
    it('should display history', () => {
      // Check if history is displayed
      cy.get('.History').should('be.visible')
  
      // Check if trivio titles are visible
      cy.get('.filter select#titleFilter').should('be.visible')
  
      // Check if pagination buttons are visible
      cy.get('.pagination button').should('be.visible')
    })
})
  