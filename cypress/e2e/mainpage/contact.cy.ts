describe('Contact Form', () => {
    beforeEach(() => {
        cy.login('test', 'password')
        cy.visit('/homepage/contact')
    })

    it('should submit the form with valid input', () => {
        // Fill in the form fields
        cy.get('#name').type('John Doe');
        cy.get('#email').type('john.doe@example.com');
        cy.get('#message').type('This is a test message');

        // Submit the form
        cy.get('#submit_button').click();

        // Check for success message
        cy.get('.response').should('contain.text', 'Message sent. Thanks for feedback!');
    });

    it('should display error message if email field is empty', () => {
        // Submit the form without filling in the email field
        cy.get('#name').type('John Doe');
        cy.get('#message').type('This is a test message');
        cy.get('#submit_button').click();

        // Check for error message
        cy.get('.response').should('contain.text', 'Email cannot be empty');
    });

    it('should display error message if message field is empty', () => {
        // Submit the form without filling in the message field
        cy.get('#name').type('John Doe');
        cy.get('#email').type('john.doe@example.com');
        cy.get('#submit_button').click();

        // Check for error message
        cy.get('.response').should('contain.text', 'Message cannot be empty');
    });
})
