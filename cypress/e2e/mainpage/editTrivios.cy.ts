describe('EditTrivio Component', () => {
    beforeEach(() => {
        cy.login('test', 'password') 
        cy.visit('/homepage/trivios')
        cy.get('.card-container .box').first().click()
        cy.get('.user-button-one').first().click()
    })

    it('should display the component with correct initial data', () => {
        // Check if the component title is displayed
        cy.contains('h1', 'Edit Trivio').should('exist')

        // Check if the initial input fields are displayed
        cy.get('.header').should('exist')
        cy.get('.input-boxes').should('exist')
        cy.get('.questions').should('exist')
        cy.get('.add-box').should('exist')

        // Check if the difficulty, category, and visibility dropdowns are displayed
        cy.get('.option').should('have.length', 3)

        // Check if the add question button is displayed
        cy.get('.add-question').should('exist')
    })

    it('should appear correct alert message and route to start page when clicking save', () => {
        // Click the save button
        cy.get('.top-button').contains('Save').click()

        // Check if the alert box is displayed with the correct message
        cy.on('window:alert', (alertMessage) => {
            expect(alertMessage).to.equal('Trivio Updated!')
        })

        // Ensure that the router navigates back to the start page after saving
        cy.url().should('include', '/start') 
    })
})
