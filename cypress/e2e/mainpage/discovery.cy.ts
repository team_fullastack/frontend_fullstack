describe('Discovery View', () => {
    beforeEach(() => {
        cy.login('test', 'password') 
    })

    it('should display trivios', () => {
        // Check if card container exists
        cy.get('.card-container').should('exist')

        // Check if at least one TrivioCard component is visible inside the card container
        cy.get('.card-container .box', { timeout: 5000 }).should('exist')
    })

    it('should display trivio with correct tags', () => {
        // Enter "Capital" in the tag input box
        cy.get('.tag-input').type('Capital')

        // Press tag button after typing the first tag
        cy.get('.tag-button').click()

        // Check if at least one TrivioCard component is visible inside the card container
        cy.get('.card-container .box', { timeout: 5000 }).should('exist')
    })

    it('should not display any trivio with incorrect tags', () => {
        // Enter "InvalidTag" in the tag input box
        cy.get('.tag-input').type('InvalidTag')

        // Press tag button after typing the first tag
        cy.get('.tag-button').click()

        // Check if no TrivioCard component is visible inside the card container
        cy.get('.card-container .box', { timeout: 5000 }).should('not.exist')
    })


    it('should display trivio with correct difficulty', () => {
        // Select "Medium" in the Difficulty dropdown
        cy.get('.FilterOptions select').eq(1).select('Easy')

        // Assert that "Medium" is selected in the Difficulty dropdown
        cy.get('.FilterOptions select').eq(1).should('have.value', 'Easy')

        // Check if at least one TrivioCard component is visible inside the card container
        cy.get('.card-container .box', { timeout: 5000 }).should('exist')
    })

    it('should display trivio with incorrect difficulty', () => {
        // Select "Medium" in the Difficulty dropdown
        cy.get('.FilterOptions select').eq(1).select('Medium')

        // Assert that "Medium" is selected in the Difficulty dropdown
        cy.get('.FilterOptions select').eq(1).should('have.value', 'Medium')

        // Check if at least one TrivioCard component is visible inside the card container
        cy.get('.card-container .box', { timeout: 5000 }).should('not.exist')
    })

    it('should navigate to start page when a TrivioCard is clicked', () => {
        // Click on the first TrivioCard
        cy.get('.card-container .box').first().click()

        // Assert that the URL has changed to the expected route
        cy.url().should('include', '/homepage/start')
    })
})
