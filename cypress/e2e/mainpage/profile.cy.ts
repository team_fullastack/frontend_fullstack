describe('Profile Component', () => {
    beforeEach(() => {
        cy.login('test', 'password');
        cy.visit('/homepage/profile');
    });

    it('fetches user information on mount', () => {
        // Check if user information is displayed correctly
        cy.get('#username').should('have.value', 'test');
        cy.get('#email').should('have.value', 'username@mail.com');
    });

    it('should allow editing user information', () => {
        // Type new username and email
        cy.get('#username').clear().type('newUsername')
        cy.get('#email').clear().type('newEmail@example.com')
    
        // Save changes
        cy.get('.user-info-header .save-button').click()
    
        // Check if changes are saved
        cy.get('#username').should('have.value', 'newUsername')
        cy.get('#email').should('have.value', 'newEmail@example.com')
      })
});
