describe('My Trivios Component', () => {
    beforeEach(() => {
        cy.login('test', 'password')
        cy.visit('/homepage/trivios') 
    })

    it('should display the filter options', () => {
        // Check if category and difficulty filter options are visible
        cy.get('.FilterOptions').should('be.visible')
        cy.get('.option').should('have.length', 4) 
    })

    it('should display the tags section', () => {
        // Check if the tags section is visible
        cy.get('.tag-section').should('be.visible')
    })

    it('should display pagination', () => {
        // Check if the pagination section exists
        cy.get('.pagination').should('exist')

        cy.get('.pagination').find('button').should('exist')
    })
})
