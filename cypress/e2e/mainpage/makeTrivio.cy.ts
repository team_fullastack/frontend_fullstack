describe('MakeTrivio Component', () => {
    beforeEach(() => {
        cy.login('test', 'password')
        cy.visit('/homepage/create-trivio')
    })

    it('should display the component with correct initial data', () => {
        // Check if the component title is displayed
        cy.contains('h1', 'Create new quiz').should('exist')

        // Check if the initial input fields are displayed
        cy.get('.header').should('exist')
        cy.get('.input-boxes').should('exist')
        cy.get('.questions').should('exist')
        cy.get('.add-box').should('exist')

        // Check if the difficulty, category, and visibility dropdowns are displayed
        cy.get('.option').should('have.length', 3)

        // Check if the add question button is displayed
        cy.get('.add-question').should('exist')
    })

    it('should add a question when the add question button is clicked', () => {
        // Click the add question button
        cy.get('.add-question').click()

        // Check if the question input boxes are displayed
        cy.get('.questions').should('have.length', 1)

        // Check if the answer input boxes are displayed
        cy.get('.answer').should('have.length', 4)
    })

    it('should appear alert box if missing title', () => {
        // Click the save button
        cy.get('.top-button').contains('Create').click()

        // Check if the alert box is displayed
        cy.on('window:alert', (alertMessage) => {
            expect(alertMessage).to.exist;
        })
    })

    it('should appear alert box if missing question', () => {
        // Type in title 
        cy.get('.trivio-title').type('Test Title')

        // Type description
        cy.get('.description').type('Test Description')

        // Click the save button
        cy.get('.top-button').contains('Create').click()

        // Check if the alert box is displayed
        cy.on('window:alert', (alertMessage) => {
            expect(alertMessage).to.exist;
        })
    })

    it('should route to my trivios page when clicking create', () => {
        // Type in title 
        cy.get('.trivio-title').type('Test Title')

        // Type description
        cy.get('.description').type('Test Description')
        // Click the add question button
        cy.get('.add-question').click()

        // Click the save button
        cy.get('.top-button').contains('Create').click()

        // Ensure that the router navigates back to the start page after saving
        cy.url().should('include', '/trivios')
    })
})
