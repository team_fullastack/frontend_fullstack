describe('Create Account Form', () => {
    beforeEach(() => {
      cy.visit('/createAccount')
    })
  
    it('should sign in with valid credentials', () => {
      // Enter valid username and password
      cy.get('#username').type('account')
      cy.get('#password').type('password')
      cy.get('#email').type('account@mail.com')
  
      // Click the sign-in button
      cy.get('#signinButton').click()
  
      // Ensure redirected to the dashboard or appropriate route after successful sign-in
      cy.url().should('include', '/homepage') 
    })
  
    it('should display error message with invalid email', () => {
      // Enter valid username and password, but invalid email
      cy.get('#username').type('username')
      cy.get('#password').type('password')
      cy.get('#email').type('InvalidEmail')
  
      // Click the sign-in button
      cy.get('#signinButton').click()
  
      // Check if error message is displayed
      cy.get('#loginStatus').should('have.text', 'Please enter a valid email address')
    })

    it('should display error message when username is empty', () => {
      // Enter empty username
      cy.get('#password').type('password')
      cy.get('#email').type('username@mail.com')
  
      // Click the sign-in button
      cy.get('#signinButton').click()
  
      // Check if error message is displayed
      cy.get('#loginStatus').should('have.text', 'Please enter a username')
    })

    it('should display error message when password is empty', () => {
      // Enter empty password
      cy.get('#username').type('username')
      cy.get('#email').type('username@mail.com')

      // Click the sign-in button
      cy.get('#signinButton').click()

      // Check if error message is displayed
      cy.get('#loginStatus').should('have.text', 'Please enter a password')
    })
  })
  