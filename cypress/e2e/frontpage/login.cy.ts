describe('Login Form', () => {
  beforeEach(() => {
    cy.visit('/') 
  })

  it('should log in with valid credentials', () => {
    // Enter valid username and password
    cy.get('#username').type('test')
    cy.get('#password').type('password')

    // Click the login button
    cy.get('#signinButton').click()

    // Ensure redirected to the homepage or appropriate route after successful login
    cy.url().should('include', '/homepage') 
  })

  it('should display error message with invalid password', () => {
    // Enter password
    cy.get('#username').type('test')
    cy.get('#password').type('pass')

    // Click the login button
    cy.get('#signinButton').click()

    // Check if error message is displayed
    cy.get('#loginStatus').should('have.text', 'Login failed')
  })
})